# Gitlab CI Workshop

## Naklonovat Nette Sandbox

```bash
$ composer create-project nette/sandbox workshop-gitlab-ci
```

## Vytvořit repozitář a pushnout do Gitlabu

```bash
$ git init
$ git add .
$ git commit -m "Initial commit"
$ git remote add origin git@gitlab.com:foxycode/workshop-gitlab-ci.git
$ git push -u origin master
```

## Základní konfigurace Gitlab CI

`app/libs/Calculator.php`
```php
<?php

namespace App\Libs;

class Calculator
{
    public function add($x, $y)
    {
        return $x + $y;
    }
}
```

`tests/libs/CalculatorTest.phpt`
```php
<?php

namespace Tests\Libs;

use App\Libs\Calculator;
use Tester;
use Tester\Assert;


require __DIR__ . '/../bootstrap.php';


class CalculatorTest extends Tester\TestCase
{
    public function testAdd()
    {
        $c = new Calculator;

        Assert::same(2, $c->add(1, 1));
        Assert::same(4, $c->add(2, 2));
    }
}


(new CalculatorTest)->run();
```

`tests/php.ini`
```ini
[PHP]
extension=tokenizer.so
extension=pdo.so
```

`.gitlab-ci.yml`
```yaml
test:
    image: sunfoxcz/docker-php-build:7.1
    before_script:
        - "composer install --no-interaction --no-progress --no-suggest --optimize-autoloader"
    script:
        - "vendor/bin/tester -s -p php -c tests/php.ini tests"
```

## Přidáme cache a config.local.neon

`.gitlab-ci.yml`
```yaml
test:
    image: sunfoxcz/docker-php-build:7.1
    before_script:
        - "cp app/config/config.test.neon app/config/config.local.neon"
        - "composer install --no-interaction --no-progress --no-suggest --optimize-autoloader"
    script:
        - "vendor/bin/tester -s -p php -c tests/php.ini tests"

cache:
    paths:
        - vendor
```

## Přidáme MySQL databázi

`app/config/config.test.neon`
```yaml
database:
    dsn: 'mysql:host=mysql;dbname=test'
    user: root
    password: aaa123
```

`.gitlab-ci.yml`
```yaml
variables:
    # Configure mysql environment variables (https://hub.docker.com/r/_/mysql/)
    MYSQL_DATABASE: test
    MYSQL_ROOT_PASSWORD: aaa123

test:
    image: sunfoxcz/docker-php-build:7.1
    services:
        - mysql:latest
    before_script:
        - "cp app/config/config.test.neon app/config/config.local.neon"
        - "composer install --no-interaction --no-progress --no-suggest --optimize-autoloader"
    script:
        - "vendor/bin/tester -s -p php -c tests/php.ini tests"

cache:
    paths:
        - vendor
```

## Přidáme PHP Parallel lint

`.gitlab-ci.yml`
```yaml
variables:
    # Configure mysql environment variables (https://hub.docker.com/r/_/mysql/)
    MYSQL_DATABASE: test
    MYSQL_ROOT_PASSWORD: aaa123

test:
    image: sunfoxcz/docker-php-build:7.1
    services:
        - mysql:latest
    before_script:
        - "composer create-project --no-interaction --no-progress --prefer-dist jakub-onderka/php-parallel-lint temp/php-parallel-lint ~0.9"
        - "cp app/config/config.test.neon app/config/config.local.neon"
        - "composer install --no-interaction --no-progress --no-suggest --optimize-autoloader"
    script:
        - "php temp/php-parallel-lint/parallel-lint.php -e php,phpt -j $(nproc) app tests"
        - "vendor/bin/tester -s -p php -c tests/php.ini tests"

cache:
    paths:
        - vendor
```

## Přidáme další verzi PHP

`.gitlab-ci.yml`
```yaml
variables:
    # Configure mysql environment variables (https://hub.docker.com/r/_/mysql/)
    MYSQL_DATABASE: test
    MYSQL_ROOT_PASSWORD: aaa123

stages:
    - download
    - test

download:
    stage: download
    image: sunfoxcz/docker-php-build:7.1
    script:
        - "composer create-project --no-interaction --no-progress --prefer-dist jakub-onderka/php-parallel-lint temp/php-parallel-lint ~0.9"
    artifacts:
        paths:
            - temp/php-parallel-lint
        expire_in: 1 hour

test:7.0:
    image: sunfoxcz/docker-php-build:7.0
    services:
        - mysql:latest
    before_script:
        - "cp app/config/config.test.neon app/config/config.local.neon"
        - "composer install --no-interaction --no-progress --no-suggest --optimize-autoloader"
    script:
        - "php temp/php-parallel-lint/parallel-lint.php -e php,phpt -j $(nproc) app tests"
        - "vendor/bin/tester -s -p php -c tests/php.ini tests"

test:7.1:
    image: sunfoxcz/docker-php-build:7.1
    services:
        - mysql:latest
    before_script:
        - "cp app/config/config.test.neon app/config/config.local.neon"
        - "composer install --no-interaction --no-progress --no-suggest --optimize-autoloader"
    script:
        - "php temp/php-parallel-lint/parallel-lint.php -e php,phpt -j $(nproc) app tests"
        - "vendor/bin/tester -s -p php -c tests/php.ini tests"

cache:
    paths:
        - vendor
```

## Vytvoříme si šablonu, aby nebyly duplicity

`.gitlab-ci.yml`
```yaml
variables:
    # Configure mysql environment variables (https://hub.docker.com/r/_/mysql/)
    MYSQL_DATABASE: test
    MYSQL_ROOT_PASSWORD: aaa123

stages:
    - download
    - test

download:
    stage: download
    image: sunfoxcz/docker-php-build:7.1
    script:
        - "composer create-project --no-interaction --no-progress --prefer-dist jakub-onderka/php-parallel-lint temp/php-parallel-lint ~0.9"
    artifacts:
        paths:
            - temp/php-parallel-lint
        expire_in: 1 hour

.test_template: &test_template
    stage: test
    services:
        - mysql:latest
    before_script:
        - "cp app/config/config.test.neon app/config/config.local.neon"
        - "composer install --no-interaction --no-progress --no-suggest --optimize-autoloader"
    script:
        - "php temp/php-parallel-lint/parallel-lint.php -e php,phpt -j $(nproc) app tests"
        - "vendor/bin/tester -s -p php -c tests/php.ini tests"

test:7.0:
    image: sunfoxcz/docker-php-build:7.0
    <<: *test_template

test:7.1:
    image: sunfoxcz/docker-php-build:7.1
    <<: *test_template

cache:
    paths:
        - vendor
```

## Přidáme deploy stage

`.gitlab-ci.yml`
```yaml
variables:
    GIT_DEPTH: "1"
    GIT_STRATEGY: fetch
    # Configure mysql environment variables (https://hub.docker.com/r/_/mysql/)
    MYSQL_DATABASE: test
    MYSQL_ROOT_PASSWORD: aaa123

stages:
    - download
    - test
    - deploy

download:
    stage: download
    image: sunfoxcz/docker-php-build:7.1
    script:
        - "composer create-project --no-interaction --no-progress --prefer-dist jakub-onderka/php-parallel-lint temp/php-parallel-lint ~0.9"
    artifacts:
        paths:
            - temp/php-parallel-lint
        expire_in: 1 hour

.test_template: &test_template
    stage: test
    services:
        - mysql:latest
    before_script:
        - "cp app/config/config.test.neon app/config/config.local.neon"
        - "composer install --no-interaction --no-progress --no-suggest --optimize-autoloader"
    script:
        - "php temp/php-parallel-lint/parallel-lint.php -e php,phpt -j $(nproc) app tests"
        - "vendor/bin/tester -s -p php -c tests/php.ini tests"
    artifacts:
        paths:
            - tests
        expire_in: 1 hour
        when: on_failure

test:7.0:
    image: sunfoxcz/docker-php-build:7.0
    <<: *test_template

test:7.1:
    image: sunfoxcz/docker-php-build:7.1
    <<: *test_template

deploy to production:
    stage: deploy
    environment: production
    image: sunfoxcz/docker-php-build:7.1
    script:
        - "curl -s -X POST https://deployer.sunfox.cz/"
    only:
        - master

cache:
    paths:
        - vendor
```

## Zdroje

* Getting started with GitLab CI: https://docs.gitlab.com/ce/ci/quick_start/
* Build Stages: https://about.gitlab.com/2016/07/29/the-basics-of-gitlab-ci/
* Job Templates: https://docs.gitlab.com/ce/ci/yaml/#special-yaml-features
* Deployer: https://github.com/REBELinBLUE/deployer
